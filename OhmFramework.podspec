
Pod::Spec.new do |spec|

  spec.name         = "OhmFramework"
  spec.version      = "1.0.8"
  spec.summary      = "OhmFramework for Schneider"
  spec.description  = "OhmFramework to be used for Schneider"

  spec.homepage     = "https://gitlab.com/anshu.vij/ohmframework"
  spec.license      = "MIT"
  spec.author             = { "Anshu Vij" => "anshu.vij@sustlabs.com" }
  spec.platform     = :ios
  spec.platform     = :ios, "13.4"
  spec.source       = { :git => "https://gitlab.com/anshu.vij/ohmframework.git", :tag => spec.version.to_s }
  spec.source_files  = "OhmFramework","OhmFramework/**/*.{swift}"
  spec.resources = "OhmFramework/**/*.{xib,png,jpeg,jpg,storyboard,xcassets}"
  #spec.exclude_files = "Classes/Exclude"
  
  spec.frameworks = "UIKit", "CoreBluetooth", "WebKit"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  spec.dependency "MBProgressHUD"
  spec.dependency "ESPProvision"
  spec.swift_versions = "5.0"

end
