//
//  StatusViewController.swift
//  OhmFramework
//
//  Created by Anshu Vij on 15/12/21.
//

import ESPProvision
import Foundation
import UIKit

class StatusViewController: UIViewController {

    @IBOutlet weak var switchIcon: UIImageView!
    @IBOutlet weak var step1Image: UIImageView!
    @IBOutlet weak var step2Image: UIImageView!
    @IBOutlet weak var okayButton: UIButton!
    @IBOutlet weak var step2Indicator: UIActivityIndicatorView!
    @IBOutlet weak var step1Indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var step1ErrorLabel: UILabel!
    @IBOutlet weak var step2ErrorLabel: UILabel!
    
    @IBOutlet weak var finalStatusLabel: UILabel!
    
    var ssid: String!
    var passphrase: String!
    var step1Failed = false
    var espDevice: ESPDevice!
    var message = ""
    var registerHandler = RegisterDeviceAPIHandler()
    
    let bundle = Bundle(for: StatusViewController.self)
    
    init() {
       super.init(nibName: "StatusViewController", bundle: Bundle(for: StatusViewController.self))
   }
   required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerHandler.delegate = self
        switchIcon.image = switchIcon.image?.withRenderingMode(.alwaysTemplate)
        switchIcon.tintColor = UIColor(red: 0, green: 163/255, blue: 52/255, alpha: 1.0)
        if step1Failed {
            step1FailedWithMessage(message: message)
        } else {
            startProvisioning()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }


    @IBAction func goToFirstView(_ sender: Any) {
        // navigationController?.popToRootViewController(animated: true)
         DispatchQueue.main.async {
             //self.navigationController?.pushViewController(WebViewController(), animated: true)
             let vc = WebViewController()
             vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
             self.present(vc, animated: true, completion: nil)

         }
     }
    
    func startProvisioning() {
        step1Image.isHidden = true
        step1Indicator.isHidden = false
        step1Indicator.startAnimating()

        espDevice.provision(ssid: ssid, passPhrase: passphrase) { status in
            DispatchQueue.main.async {
                switch status {
                case .success:
                    self.step2Indicator.stopAnimating()
                    self.step2Image.image = UIImage(named: "tick-mark.png",in: self.bundle, compatibleWith: nil)
                    self.step2Image.isHidden = false
                    self.provisionFinsihedWithStatus(message: "Device has been successfully provisioned!")
                    let espString = self.espDevice.name
                    let idSep = espString.components(separatedBy: "wiser_")
                    debugPrint("mac id in status is :\(idSep[1])")
                    debugPrint("url in status vc is :https://prod.wiser.sustlabs.com/secure/idfMappingRequest/\(idSep[1])")
                 //   Utility.showLoader(message: "", view: self.view)
//                    self.registerHandler.registerDevice(url: "https://prod.wiser.sustlabs.com/secure/idfMappingRequest/\(idSep[1])")
                    
                    
                    
                case let .failure(error):
                    switch error {
                    case .configurationError:
                        self.step1FailedWithMessage(message: "Failed to apply network configuration to device")
                    case .sessionError:
                        self.step1FailedWithMessage(message: "Session is not established")
                    case .wifiStatusDisconnected:
                        self.step2FailedWithMessage(error: error)
                    default:
                        self.step2FailedWithMessage(error: error)
                    }
                case .configApplied:
                    self.step2applyConfigurations()
                }
            }
        }
    }

    func step2applyConfigurations() {
        DispatchQueue.main.async {
            self.step1Indicator.stopAnimating()
            self.step1Image.image = UIImage(named: "tick-mark.png",in: self.bundle, compatibleWith: nil)
            self.step1Image.isHidden = false
            self.step2Image.isHidden = true
            self.step2Indicator.isHidden = false
            self.step2Indicator.startAnimating()
        }
    }

    func step1FailedWithMessage(message: String) {
        DispatchQueue.main.async {
            self.step1Indicator.stopAnimating()
            self.step1Image.image = UIImage(named: "error_icon.png",in: self.bundle, compatibleWith: nil)
            self.step1Image.isHidden = false
            self.step1ErrorLabel.text = message
            self.step1ErrorLabel.isHidden = false
            self.provisionFinsihedWithStatus(message: "Reboot your board and try again.")
        }
    }

    func step2FailedWithMessage(error: ESPProvisionError) {
        DispatchQueue.main.async {
            self.step2Indicator.stopAnimating()
            self.step2Image.image = UIImage(named: "error_icon.png",in: self.bundle, compatibleWith: nil)
            self.step2Image.isHidden = false
            var errorMessage = ""
            switch error {
            case .wifiStatusUnknownError, .wifiStatusDisconnected, .wifiStatusNetworkNotFound, .wifiStatusAuthenticationError:
                errorMessage = error.description
            case .wifiStatusError:
                errorMessage = "Unable to fetch Wi-Fi state."
            default:
                errorMessage = "Unknown error."
            }
            self.step2ErrorLabel.text = errorMessage
            self.step2ErrorLabel.isHidden = false
            self.provisionFinsihedWithStatus(message: "Reset your board to factory defaults and retry.")
        }
    }

    func provisionFinsihedWithStatus(message: String) {
        okayButton.isEnabled = true
        okayButton.alpha = 1.0
        finalStatusLabel.text = message
        finalStatusLabel.isHidden = false
    }
}

extension StatusViewController : RegisterDeviceAPIHandlerDelegate {
    func okStatus(_ LoginAPIHandler: RegisterDeviceAPIHandler) {
        DispatchQueue.main.async {
            Utility.hideLoader(view: self.view)
            let vc = WebViewController()
            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(vc, animated: true, completion: nil)
            //self.navigationController?.pushViewController(WebViewController(), animated: true)
        }
    }
    
    func didFailWithError(error: String) {
        
        
        DispatchQueue.main.async {
            Utility.hideLoader(view: self.view)
            let alertController = UIAlertController(title: "Error!", message: "Something went wrong!", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
}
