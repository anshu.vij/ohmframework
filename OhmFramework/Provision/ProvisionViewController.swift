//
//  ProvisionViewController.swift
//  OhmFramework
//
//  Created by Anshu Vij on 15/12/21.
//

import CoreBluetooth
import ESPProvision
import Foundation
import MBProgressHUD
import UIKit


class ProvisionViewController: UIViewController {

    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var provisionButton: UIButton!
    @IBOutlet weak var ssidTextfield: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var passphraseTextfield: UITextField!
    
    @IBOutlet weak var headerLabel: UILabel!
    
    
    var activityView: UIActivityIndicatorView?
    var wifiDetailList: [ESPWifiNetwork] = []
    var alertTextField: UITextField?
    var showPasswordImageView: UIImageView!
    var espDevice: ESPDevice!
    var passphrase = ""
    var ssid = ""
    var attemptPair = AttemptPairAPI()
    var tempToken : AttemptModel?
    
    let bundle = Bundle(for: ProvisionViewController.self)
    
    init() {
       super.init(nibName: "ProvisionViewController", bundle: Bundle(for: ProvisionViewController.self))
   }
   required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.text = "To continue setup of your device \(espDevice.name), please provide your Home Network's credentials."
        // Remove back button, user will have the option to cancel the provisioning.
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        // Setup show/hide password feature for passphrase textfield.
        showPasswordImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let tap = UITapGestureRecognizer(target: self, action: #selector(showPassword))
        tap.numberOfTapsRequired = 1
        showPasswordImageView.isUserInteractionEnabled = true
        showPasswordImageView.contentMode = .scaleAspectFit
        showPasswordImageView.addGestureRecognizer(tap)
        
        configurePassphraseTextField()

        // Add keyboard notification for texfield inputs
        passphraseTextfield.addTarget(self, action: #selector(passphraseEntered), for: .editingDidEndOnExit)
        ssidTextfield.addTarget(self, action: #selector(ssidEntered), for: .editingDidEndOnExit)
        provisionButton.isUserInteractionEnabled = false

        // Setup table view
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)

       scanDeviceForWiFiList()
        
        tableView.register(UINib(nibName: "WifiListTableViewCell", bundle: bundle), forCellReuseIdentifier: "WifiListTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        attemptPair.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if let token = UserDefaults.standard.string(forKey: "token") {
            let espString = self.espDevice.name
            let idSep = espString.components(separatedBy: "wiser_")
            debugPrint("mac id in status is :\(idSep[1])")
        self.attemptPair.attemptToPair(authCode: token, macID: idSep[1])
            
        }
    }
    
    
    
    @IBAction func cancelClicked(_ sender: Any) {
        
        espDevice.disconnect()
        navigationController?.popToRootViewController(animated: false)
    }
    

    @IBAction func provisionButtonClicked(_ sender: Any) {
        guard let ssid = ssidTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines), let passphrase = passphraseTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines),
            ssid.count > 0, passphrase.count > 0 else {
            return
        }
        provisionDevice(ssid: ssid, passphrase: passphrase)
    }
    
    
    @IBAction func rescanWiFiList(_ sender: Any) {
        
        scanDeviceForWiFiList()
    }
    
    // MARK: - Provision

    // Start provisioning
    func provisionDevice(ssid: String, passphrase: String) {
        self.ssid = ssid
        self.passphrase = passphrase
        showStatusScreen()
    }

    // Scan device for available Wi-Fi networks.
   func scanDeviceForWiFiList()
    {
    debugPrint("I am in scanDevice")
    DispatchQueue.main.async {
        self.tableView.isHidden = false
        self.headerView.isHidden = false
        Utility.hideLoader(view: self.view)
            //                if let list = wifiList {
            //                    self.wifiDetailList = list.sorted { $0.rssi > $1.rssi }
            //                }
            self.tableView.reloadData()
        }
    }
    
    //{
//        debugPrint("I am in scanDevice")
//        Utility.showLoader(message: "Scanning for Wi-Fi", view: view)
//        espDevice.scanWifiList { wifiList, _ in
//            DispatchQueue.main.async {
//                self.tableView.isHidden = false
//                self.headerView.isHidden = false
//                Utility.hideLoader(view: self.view)
////                if let list = wifiList {
////                    self.wifiDetailList = list.sorted { $0.rssi > $1.rssi }
////                }
//                self.tableView.reloadData()
//            }
//        }
//    }

    @objc func passphraseEntered() {
        passphraseTextfield.resignFirstResponder()
        guard let ssid = ssidTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines), let passphrase = passphraseTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        if ssid.count > 0, passphrase.count > 0 {
            provisionButton.isUserInteractionEnabled = true
            provisionDevice(ssid: ssid, passphrase: passphrase)
        }
    }

    @objc func ssidEntered() {
        guard let ssid = ssidTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines), let passphrase = passphraseTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            return
        }
        if ssid.count > 0, passphrase.count > 0 {
            provisionButton.isUserInteractionEnabled = true
        }
        passphraseTextfield.becomeFirstResponder()
    }

    // Set Wi-Fi signal and security image based on rssi value and security.
    func setWifiIconImageFor(cell: WifiListTableViewCell, network: ESPWifiNetwork) {
        let rssi = network.rssi
        if rssi > Int32(-50) {
            
            cell.signalImageView.image = UIImage(named: "wifi_symbol_strong.png",in: bundle, compatibleWith: nil)

        } else if rssi > Int32(-60) {
            cell.signalImageView.image = UIImage(named: "wifi_symbol_good.png",in: bundle, compatibleWith: nil)
        } else if rssi > Int32(-67) {
            cell.signalImageView.image = UIImage(named: "wifi_symbol_fair.png",in: bundle, compatibleWith: nil)
        } else {
            cell.signalImageView.image = UIImage(named: "wifi_symbol_weak.png",in: bundle, compatibleWith: nil)
        }
        if network.auth != .open {
            cell.authenticationImageView.isHidden = false
        } else {
            cell.authenticationImageView.isHidden = true
        }
    }

    // Method to join hidden networks which are not visible on Wi-Fi list
    func joinOtherNetwork() {
        let input = UIAlertController(title: "", message: nil, preferredStyle: .alert)

        input.addTextField { textField in
            textField.placeholder = "Network Name"
            self.addHeightConstraint(textField: textField)
        }

        input.addTextField { textField in
            self.configurePasswordTextfield(textField: textField)
            self.addHeightConstraint(textField: textField)
        }
        input.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
        }))
        input.addAction(UIAlertAction(title: "Connect", style: .default, handler: { [weak input] _ in
            let ssidTextField = input?.textFields![0]
            let passphrase = input?.textFields![1]

            if let ssid = ssidTextField?.text, ssid.count > 0 {
                self.provisionDevice(ssid: ssid, passphrase: passphrase?.text ?? "")
            }
        }))
        DispatchQueue.main.async {
            self.present(input, animated: true, completion: nil)
        }
    }

    // MARK: - UIConfiguration
    
    func addHeightConstraint(textField: UITextField) {
        let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
        textField.addConstraint(heightConstraint)
        textField.font = UIFont(name: textField.font!.fontName, size: 18)
    }

    func configurePasswordTextfield(textField: UITextField) {
        alertTextField = textField
        textField.placeholder = "Password"
        textField.isSecureTextEntry = true
        showPasswordImageView.image = UIImage(named: "show_password.png",in: bundle, compatibleWith: nil)
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: showPasswordImageView.frame.width + 10, height: showPasswordImageView.frame.height))
        rightView.addSubview(showPasswordImageView)
        textField.rightView = rightView
        textField.rightViewMode = .always
    }

    func configurePassphraseTextField() {
        alertTextField = passphraseTextfield
        passphraseTextfield.placeholder = "Password"
        passphraseTextfield.isSecureTextEntry = true
        showPasswordImageView.image = UIImage(named: "show_password.png",in: bundle, compatibleWith: nil)
        let rightView = UIView(frame: CGRect(x: 0, y: 0, width: showPasswordImageView.frame.width + 10, height: showPasswordImageView.frame.height))
        rightView.addSubview(showPasswordImageView)
        passphraseTextfield.rightView = rightView
        passphraseTextfield.rightViewMode = .always
    }
    
    @objc func showPassword() {
        if let secureEntry = self.alertTextField?.isSecureTextEntry {
            alertTextField?.togglePasswordVisibility()
            if secureEntry {
                showPasswordImageView.image = UIImage(named: "hide_password.png",in: bundle, compatibleWith: nil)
            } else {
                showPasswordImageView.image = UIImage(named: "show_password.png",in: bundle, compatibleWith: nil)
            }
        }
    }

    // MARK: - Helper Methods
    
    func showStatusScreen() {
        DispatchQueue.main.async {
            Utility.hideLoader(view: self.view)
            let statusVC = StatusViewController()
            statusVC.ssid = self.ssid
            statusVC.passphrase = self.passphrase
            statusVC.espDevice = self.espDevice
            statusVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(statusVC, animated: true, completion: nil)
            //self.navigationController?.pushViewController(statusVC, animated: true)
        }
    }
}

extension ProvisionViewController: UITableViewDelegate {
    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row >= wifiDetailList.count {
            joinOtherNetwork()
        } else {
            let wifiNetwork = wifiDetailList[indexPath.row]
            ssid = wifiNetwork.ssid

            if wifiNetwork.auth != .open {
                let input = UIAlertController(title: ssid, message: nil, preferredStyle: .alert)

                input.addTextField { textField in
                    self.configurePasswordTextfield(textField: textField)
                    self.addHeightConstraint(textField: textField)
                }
                input.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in

                }))
                input.addAction(UIAlertAction(title: "Provision", style: .default, handler: { [weak input] _ in
                    let textField = input?.textFields![0]
                    guard let passphrase = textField?.text else {
                        return
                    }
                    if passphrase.count > 0 {
                        self.provisionDevice(ssid: self.ssid, passphrase: passphrase)
                    }
                }))
                present(input, animated: true, completion: nil)
            } else {
                provisionDevice(ssid: ssid, passphrase: "")
            }
        }
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 60.0
    }
}

extension ProvisionViewController: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return wifiDetailList.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WifiListTableViewCell", for: indexPath) as! WifiListTableViewCell
        if indexPath.row >= wifiDetailList.count {
            debugPrint("Join Other Network")
            cell.ssidLabel.text = "Join Other Network"
            cell.authenticationImageView.isHidden = true
            cell.signalImageView.isHidden = true
        } else {
            debugPrint("Join n/w")
            cell.signalImageView.isHidden = false
            cell.ssidLabel.text = wifiDetailList[indexPath.row].ssid
            setWifiIconImageFor(cell: cell, network: wifiDetailList[indexPath.row])
        }
        return cell
    }
}

extension ProvisionViewController : AttemptPairDelegate {
    func okStatus(_ APIHandler: AttemptPairAPI, data: AttemptModel) {
        
        debugPrint(data.tempToken)
        DispatchQueue.main.async {
            Utility.hideLoader(view: self.view)
        }
        let data = Data(data.tempToken.utf8)
        espDevice.sendData(path: "auth-data", data: data) { data, error in
            debugPrint("error code:" + "\(error?.code)")
            debugPrint("error code:" + "\(error?.description)")
        }
        
    }
    
    func didFailWithError(error: Error) {
        
    }
    
    
}
