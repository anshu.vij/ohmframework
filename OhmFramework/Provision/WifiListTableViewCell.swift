//
//  WifiListTableViewCell.swift
//  OhmFramework
//
//  Created by Anshu Vij on 15/12/21.
//

import UIKit

class WifiListTableViewCell: UITableViewCell {

    @IBOutlet weak var signalImageView: UIImageView!
    @IBOutlet weak var ssidLabel: UILabel!
    @IBOutlet weak var authenticationImageView: UIImageView!
    
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
