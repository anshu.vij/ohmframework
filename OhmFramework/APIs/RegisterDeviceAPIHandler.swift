//
//  RegisterDeviceAPIHandler.swift
//  OhmFramework
//
//  Created by Anshu Vij on 16/12/21.
//

import Foundation
import UIKit

protocol RegisterDeviceAPIHandlerDelegate {
    func okStatus(_ LoginAPIHandler : RegisterDeviceAPIHandler)
    func didFailWithError(error : String)
}

struct RegisterDeviceAPIHandler {
    
    var delegate : RegisterDeviceAPIHandlerDelegate?
    func registerDevice(url : String)
    {
       
        performRequest(with: url)
    }
    
    
    
   private func performRequest(with urlString : String)
    {
        
      
        if let url  = URL(string: urlString)
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
           
    
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                
                if error != nil {
                    self.delegate?.didFailWithError(error: "Error")
                    return
                }
                
                guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                    //debugPrint("status code :\(response)")
                    debugPrint("Error: HTTP request failed")
                    self.delegate?.didFailWithError(error: "Something went wrong!")
                    return
                }
                debugPrint("status code :\(response.statusCode)")
                    self.delegate?.okStatus(self)
                    
                
                
            }
            
            task.resume()
        }
        
    }
    
    
    
    func parseJSON( _ applianceData : Data) -> LoginModel
    {
        var updatedApplianceModel: LoginModel?
        let decoder = JSONDecoder()
        do {
            let decodeData : LoginModel? = try decoder.decode(LoginModel.self, from: applianceData)
            updatedApplianceModel = decodeData
        }
        
        
        catch DecodingError.dataCorrupted(let context) {
            print(context)
            self.delegate?.didFailWithError(error: "Error")
        } catch DecodingError.keyNotFound(let key, let context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            self.delegate?.didFailWithError(error: "Error")
        } catch DecodingError.valueNotFound(let value, let context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            self.delegate?.didFailWithError(error: "Error")
        } catch DecodingError.typeMismatch(let type, let context) {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
            self.delegate?.didFailWithError(error: "Error")
        }
        catch {
            self.delegate?.didFailWithError(error: "Error")
            
            
        }
        
        return updatedApplianceModel ?? LoginModel(_id: "", email: "", meters: [] , token: "")
    }
    
    
}
