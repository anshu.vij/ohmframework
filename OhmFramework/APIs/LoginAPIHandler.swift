//
//  LoginAPIHandler.swift
//  SchneiderDemo
//
//  Created by Anshu Vij on 23/09/21.
//

import Foundation
import UIKit

protocol LoginAPIHandlerDelegate {
    func getLoginData(_ LoginAPIHandler : LoginAPIHandler, data : LoginModel)
    func didFailWithError(error : String)
}

struct LoginAPIHandler{
    
    var delegate : LoginAPIHandlerDelegate?
    func sendApplianceData(url : String)
    {
       
        performRequest(with: url)
    }
    
    
    
   private func performRequest(with urlString : String)
    {
        
      
        if let url  = URL(string: urlString)
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
           
    
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let httpResponse = response as? HTTPURLResponse {
                    debugPrint("error \(httpResponse.statusCode)")
                }
                if error != nil {
                    self.delegate?.didFailWithError(error: "Error")
                    return
                }
                
                if let safeData = data
                {
                    print("applianceData : \(safeData)")
                    
                    
                    let applianceData = self.parseJSON(safeData)
                    
                    self.delegate?.getLoginData(self, data: applianceData)
                    
                }
                
            }
            
            task.resume()
        }
        
    }
    
    
    
    func parseJSON( _ applianceData : Data) -> LoginModel
    {
        var updatedApplianceModel: LoginModel?
        let decoder = JSONDecoder()
        do {
            let decodeData : LoginModel? = try decoder.decode(LoginModel.self, from: applianceData)
            updatedApplianceModel = decodeData
        }
        
        
        catch DecodingError.dataCorrupted(let context) {
            print(context)
            self.delegate?.didFailWithError(error: "Error")
        } catch DecodingError.keyNotFound(let key, let context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            self.delegate?.didFailWithError(error: "Error")
        } catch DecodingError.valueNotFound(let value, let context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            self.delegate?.didFailWithError(error: "Error")
        } catch DecodingError.typeMismatch(let type, let context) {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
            self.delegate?.didFailWithError(error: "Error")
        }
        catch {
            self.delegate?.didFailWithError(error: "Error")
            
            
        }
        
        return updatedApplianceModel ?? LoginModel(_id: "", email: "", meters: [] , token: "")
    }
    
    
}


