//
//  LoginModel.swift
//  SchneiderDemo
//
//  Created by Anshu Vij on 23/09/21.
//

import Foundation


struct LoginModel : Codable {
    
    let _id : String
    let email : String
    let meters : [String]
    let token : String
}


struct AttemptModel : Codable {
    
    let tempToken : String
}
