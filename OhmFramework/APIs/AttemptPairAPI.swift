//
//  AttemptPairAPI.swift
//  OhmFramework
//
//  Created by Anshu Vij on 20/02/22.
//

import Foundation
import UIKit


protocol AttemptPairDelegate {
    
    func okStatus(_ APIHandler : AttemptPairAPI, data : AttemptModel)
    func didFailWithError(error : Error)
}
struct AttemptPairAPI {
    
    var delegate : AttemptPairDelegate?
    
    func attemptToPair(authCode : String, macID : String) {
        
       // let url =  "https://ppr.wiser.sustlabs.com/secure/attemptPairingV2"
        let url =  "https://prod.wiser.sustlabs.com/secure/attemptPairingV2"
        
        performRequest(with: url, authCode: authCode, macID: macID)
        
        
    }
    
    private func performRequest(with urlString : String, authCode : String, macID : String)
    {
        
        
        if let url  = URL(string: urlString)
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            request.setValue(authCode, forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("SE_cIAM+SUST_AUTH", forHTTPHeaderField: "x-auth-type")
           // request.setValue(macID, forHTTPHeaderField: "device_mac")
            let dataVal = ["device_mac" : macID]
            let jsonData = try? JSONSerialization.data(withJSONObject: dataVal, options: [])
            if let jsonString = String(data: jsonData!, encoding: String.Encoding.utf8) {
                debugPrint("Attempt pair request "  + jsonString)
            }
            //debugPrint(jsonData?.description)
          //  debugPrint(jsonData?.count)
            request.httpBody = jsonData

            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                //print(error)
                if error != nil {
                    //  self.delegate?.didFailWithError(error: error!)
                    print(error?.localizedDescription ?? "error")
                    return
                }
                guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                    print("Error: HTTP request failed")
                    //                                print(data)
                    return
                }
                        
                
                if let safeData = data
                {
                    
                    print("addData:\(safeData)")
                    let data = self.parseJSON(safeData)
                    
                    self.delegate?.okStatus(self, data: data)
                }
                
            }
            
            task.resume()
        }
        
    }
    
    
    
   private func parseJSON( _ applianceData : Data) -> AttemptModel
    {
        var updatedApplianceModel:AttemptModel?
        let decoder = JSONDecoder()
        do {
            let decodeData : AttemptModel = try decoder.decode(AttemptModel.self, from: applianceData)
            updatedApplianceModel = decodeData
        }


        catch DecodingError.dataCorrupted(let context) {
            print(context)
        } catch DecodingError.keyNotFound(let key, let context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch DecodingError.valueNotFound(let value, let context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch DecodingError.typeMismatch(let type, let context) {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
        }
        catch {
            self.delegate?.didFailWithError(error: error)


        }

        return updatedApplianceModel ?? AttemptModel(tempToken: "")
    }
    
}
