//
//  Credentials.swift
//  OhmFramework
//
//  Created by Anshu Vij on 15/12/21.
//


import Foundation

@objc public protocol CredentialsDelegate
{
    
}
@objc open class Credentials: NSObject
    
{
    open var BFO: String!
    open var MAIN_URL: String!
    open var MODE :String!
   
 
   /// open
    var delegate : CredentialsDelegate?
    

    
    open func getBFOToken() -> String
        
    {
        return BFO!
    }
   
    
    open func getMAIN_URL() -> String
        
    {
        return MAIN_URL!
    }
    
    open func getMODE() -> String
        
    {
        return MODE!
    }
    

}
