//
//  AppLauncherVC.swift
//  OhmFramework
//
//  Created by Anshu Vij on 09/12/21.
//

import UIKit

public class AppLauncherVC: UIViewController {
    
    open var credential = Credentials()
    var loginAPiHandler = LoginAPIHandler()
    
    public init() {
        super.init(nibName: "AppLauncherVC", bundle: Bundle(for: AppLauncherVC.self))
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       loginAPiHandler.delegate = self
    }
    
//    public override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
////        debugPrint("I am here url :\(credential.MAIN_URL)")
////        debugPrint("I am here :\(credential.getMAIN_URL())")
//
//        let url = credential.getMAIN_URL()
//        if url.count > 0 {
//            //debugPrint(url)
//            Utility.showLoader(message: "", view: self.view)
//            loginAPiHandler.sendApplianceData(url: url)
//        }
//    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        debugPrint("I am here url :\(credential.MAIN_URL)")
//        debugPrint("I am here :\(credential.getMAIN_URL())")
        
        let bfoToken = credential.getBFOToken()
        if bfoToken.count > 0 {
            
            Utility.showLoader(message: "", view: self.view)
            var url : String
            print("mode in viewWillAppear :\(credential.getMODE())")
            if(credential.getMODE() == "prod")
            {
                url = "https://prod.wiser.sustlabs.com/getToken?BFO="+bfoToken
            }
            else
            {
                url = "https://ppr.wiser.sustlabs.com/getToken?BFO="+bfoToken
            }
            debugPrint(url)
            loginAPiHandler.sendApplianceData(url: url)
        }
    }

}

extension AppLauncherVC : LoginAPIHandlerDelegate {
    func getLoginData(_ LoginAPIHandler: LoginAPIHandler, data: LoginModel) {
      //https://se-wiser.web.app/loading?env=ppr&platform=ios&token=123
        debugPrint("loginData :\(data)")
        UserDefaults.standard.setValue(data.token, forKey: "token")
        var urlString = ""
        //let urlString = "https://se-wiser.web.app/loading?token=\(data.token)"
        
        debugPrint("MODE :" + credential.getMODE())
        if(credential.getMODE() == "prod")
        {
            debugPrint("prod mode")
            urlString = "https://se-wiser.web.app/loading?env=prod&platform=ios&token=\(data.token)"
        }
        else
        {
            debugPrint("ppr mode")
            urlString = "https://se-wiser.web.app/loading?env=ppr&platform=ios&token=\(data.token)"
        }
        debugPrint("urlString :\(urlString)")
        
        UserDefaults.standard.setValue(urlString, forKey: "url")
        DispatchQueue.main.async {
            Utility.hideLoader(view: self.view)
            if data.meters.count > 0 {
                let controller = WebViewController()
                controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(controller, animated: true, completion: nil)
               // self.navigationController?.pushViewController(WebViewController(), animated: true)
            }
            else {
                let controller = BLELandingViewController()
                controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(controller, animated: true, completion: nil)
                //self.navigationController?.pushViewController(BLELandingViewController(), animated: true)
            }

        }
       
    }
    
    func didFailWithError(error: String) {
        
        debugPrint("loginerror :\(error)")
        DispatchQueue.main.async {
            Utility.hideLoader(view: self.view)
            let alertController = UIAlertController(title: "Error!", message: "Something went wrong!", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    
}

