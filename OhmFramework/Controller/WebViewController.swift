//
//  WebViewController.swift
//  SchneiderDemo
//
//  Created by Anshu Vij on 23/09/21.
//

import UIKit
import WebKit

class WebViewController: UIViewController,WKNavigationDelegate,WKUIDelegate, WKScriptMessageHandler {
    
    
    
    @IBOutlet weak var webKitView: WKWebView!
    public var url:String?
    
    
//    let content = """
//      <!DOCTYPE html><html><body>
//      <button onclick="onClick()">Click me</button>
//      <script>
//      function onClick() {
//        window.webkit.messageHandlers.backHomePage.postMessage("success");
//      }
//      </script>
//      </body></html>
//      """

    init() {
           super.init(nibName: "WebViewController", bundle: Bundle(for: WebViewController.self))
       }
       required init?(coder aDecoder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
        public override func viewDidLoad() {
            
            
            navigationController?.navigationBar.isHidden = false
            self.navigationItem.setHidesBackButton(true, animated: false)

            let logOut = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logOutTapped))
            self.webKitView.uiDelegate = self
            self.webKitView.navigationDelegate = self
            navigationItem.rightBarButtonItems = [logOut]
            if let url = UserDefaults.standard.string(forKey: "url") {
                let url = URL(string: url)!
                debugPrint(url)
                //webKitView.configuration = WKWebViewConfiguration()
                URLCache.shared.removeAllCachedResponses()
                URLCache.shared.diskCapacity = 0
                URLCache.shared.memoryCapacity = 0
                webKitView.configuration.userContentController.add(self, name: "backHomePage")
                webKitView.configuration.userContentController.add(self, name: "supportEmail")
                webKitView.configuration.userContentController.add(self, name: "openPairing")
                webKitView.configuration.userContentController.add(self, name: "exit")

                webKitView.load(URLRequest(url: url))
                webKitView.allowsBackForwardNavigationGestures = true
            }
    //        var prefs = webKitView.
    //        prefs?._setLocalStorageDatabasePath("~/Library/Application Support/MyApp")
    //        prefs?.localStorageEnabled = true
    //        let config = WKWebViewConfiguration()
    //            config.userContentController = WKUserContentController()
    //            config.userContentController.add(self, name: "backHomePage")
    ////
    ////            let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: 200, height: 200), configuration: config)
    ////
    ////            view.addSubview(webView)
    //      webKitView.configuration.userContentController.add(self, name: "backHomePage")
    ////        webKitView.loadHTMLString(content, baseURL: nil)
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
        
        @objc func logOutTapped() {
            
            UserDefaults.standard.removeObject(forKey: "url")
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        @available(iOS 14.5, *)
        func webView(_ webView: WKWebView, navigationAction: WKNavigationAction, didBecome download: WKDownload) {
            
            
            
        }
        
        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            
            if (navigationAction.navigationType == .linkActivated){
                    decisionHandler(.cancel)
                } else {
                    decisionHandler(.allow)
                }
            
        }
        
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            
            debugPrint("message name :\(message.name)")
            debugPrint("message value :\(message.body)")
        switch( message.name ) {
            case "openPairing" :
                debugPrint("trigger to open pairing again")
            
            DispatchQueue.main.async {
                let controller = BLELandingViewController()
                controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(controller, animated: true, completion: nil)
            }
            
            
            case "supportEmail" :
            
            let str = message.body as! String
            
            if let url = URL(string: str)
            {
            if url.scheme == "mailto",
                let components = URLComponents(url: url, resolvingAgainstBaseURL: false) {
                let email = components.path
                print(email)  // "test@test.com\n"
                if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
            }
            }
            
            case "backHomePage" :
                if let url = message.body as? String {
                    guard let url = URL(string: url) else { return }
                    UIApplication.shared.open(url)
                }
            case "exit" :
                let controller = BLELandingViewController()
                controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(controller, animated: true, completion: nil)
            
            default:
                break
        }
            
    //        if let url = message.body as? String {
    //        guard let url = URL(string: url) else { return }
    //        UIApplication.shared.open(url)
    //        }
            
        }
        
        
        
        

    }
