//
//  BLEDeviceListViewCell.swift
//  OhmFramework
//
//  Created by Anshu Vij on 10/12/21.
//

import UIKit

class BLEDeviceListViewCell: UITableViewCell {

    @IBOutlet weak var deviceName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
