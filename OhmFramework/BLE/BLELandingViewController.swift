//
//  BLELandingViewController.swift
//  OhmFramework
//
//  Created by Anshu Vij on 10/12/21.
//

import CoreBluetooth
import Foundation
import MBProgressHUD
import UIKit
import ESPProvision

protocol BLEStatusProtocol {
    func peripheralDisconnected()
}


class BLELandingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var activityView: UIActivityIndicatorView?
    var grayView: UIView?
    var delegate: BLEStatusProtocol?
    var bleConnectTimer = Timer()
    var bleDeviceConnected = false
    var bleDevices:[ESPDevice]?
    var pop = ""
    var espDevice: ESPDevice!
    
    
    
    @IBOutlet weak var textTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var prefixlabel: UILabel!
    @IBOutlet weak var prefixTextField: UITextField!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var prefixView: UIView!
    
     init() {
        super.init(nibName: "BLELandingViewController", bundle: Bundle(for: BLELandingViewController.self))
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.title = "Connect"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        // Scan for bluetooth devices

        // UI customization
        prefixlabel.layer.masksToBounds = true
        tableview.tableFooterView = UIView()

        // Adding tap gesture to hide keyboard on outside touch
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

        // Checking whether filtering by prefix header is allowed
        prefixTextField.text = Utility.shared.deviceNamePrefix
        if Utility.shared.espAppSettings.allowPrefixSearch {
            prefixView.isHidden = false
        } else {
            textTopConstraint.constant = -10
            view.layoutIfNeeded()
        }
        
        scanBleDevices()

        let bundle = Bundle(for: BLELandingViewController.self)
        tableview.register(UINib(nibName: "BLEDeviceListViewCell", bundle: bundle), forCellReuseIdentifier: "BLEDeviceListViewCell")
        
        tableview.delegate = self
        tableview.dataSource = self
        prefixTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prefixView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    
    
    @IBAction func rescanBLEDevices(_ sender: Any) {
        bleDevices?.removeAll()
        tableview.reloadData()
        scanBleDevices()
    }
    
    func scanBleDevices() {
        Utility.showLoader(message: "Searching for BLE Devices..", view: self.view)
        ESPProvisionManager.shared.searchESPDevices(devicePrefix: Utility.shared.deviceNamePrefix, transport: .ble) { bleDevices, error in
            DispatchQueue.main.async {
                Utility.hideLoader(view: self.view)
                self.bleDevices = bleDevices
                self.tableview.reloadData()
            }
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @objc func keyboardWillDisappear() {
        if let prefix = prefixTextField.text {
            UserDefaults.standard.set(prefix, forKey: "com.espressif.prefix")
            Utility.shared.deviceNamePrefix = prefix
            rescanBLEDevices(self)
        }
    }
    
    // MARK: - Helper Methods
    
    func goToProvision(device: ESPDevice) {
        DispatchQueue.main.async {
            Utility.hideLoader(view: self.view)
            let provisionVC = ProvisionViewController()
            provisionVC.espDevice = device
           //let vc = WebViewController()
            provisionVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(provisionVC, animated: true, completion: nil)
            //self.navigationController?.pushViewController(provisionVC, animated: true)
        }
    }
    
    private func showAlert(error: String, action: UIAlertAction) {
        let alertController = UIAlertController(title: "Error!", message: error, preferredStyle: .alert)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func connectDevice(device:ESPDevice) {
        device.connect(delegate: self) { status in
            DispatchQueue.main.async {
                Utility.hideLoader(view: self.view)
            }
            switch status {
            case .connected:
                DispatchQueue.main.async {
                   // device.security = Utility.shared.espAppSettings.securityMode
                    self.goToProvision(device: device)
                }
            case let .failedToConnect(error):
                DispatchQueue.main.async {
                    var errorDescription = ""
                    switch error {
                    case .securityMismatch, .versionInfoError:
                        errorDescription = error.description
                    default:
                        errorDescription = error.description + "\nCheck if POP is correct."
                    }
                    let action = UIAlertAction(title: "Retry", style: .default, handler: nil)
                    self.showAlert(error: errorDescription, action: action)
                }
            default:
                DispatchQueue.main.async {
                    let action = UIAlertAction(title: "Retry", style: .default, handler: nil)
                    self.showAlert(error: "Device disconnected", action: action)
                }
            }
        }
    }
    
    // MARK: - UITableView
    
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        guard let peripherals = self.bleDevices else {
            return 0
        }
        return peripherals.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BLEDeviceListViewCell", for: indexPath) as! BLEDeviceListViewCell
        if let peripheral = self.bleDevices?[indexPath.row] {
            cell.deviceName.text = peripheral.name
        }

        return cell
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        Utility.showLoader(message: "Connecting to device", view: self.view)
        let espString = self.bleDevices![indexPath.row]
        let idSep = espString.name.components(separatedBy: "wiser_")
        debugPrint("mac id is :\(idSep[1])")
        self.connectDevice(device: self.bleDevices![indexPath.row])
    }
    
    func goToProvision() {
//        DispatchQueue.main.async {
//            Utility.hideLoader(view: self.view)
//            let provisionVC = self.storyboard?.instantiateViewController(withIdentifier: "provision") as! ProvisionViewController
//            provisionVC.espDevice = self.espDevice
//            self.navigationController?.pushViewController(provisionVC, animated: true)
//        }
    }
    
    func showStatusScreen(error: ESPSessionError) {
//            let statusVC = self.storyboard?.instantiateViewController(withIdentifier: "statusVC") as! StatusViewController
//            statusVC.espDevice = self.espDevice
//            statusVC.step1Failed = true
//            statusVC.message = error.description
//            self.navigationController?.pushViewController(statusVC, animated: true)

    }


    // MARK: - Helper Methods
    
//    func showAlert(error: String, action: UIAlertAction) {
//        let alertController = UIAlertController(title: "Error!", message: error, preferredStyle: .alert)
//        alertController.addAction(action)
//        present(alertController, animated: true, completion: nil)
//    }
    
}

extension BLELandingViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
}

extension BLELandingViewController: ESPDeviceConnectionDelegate {
    
    
//    func getProofOfPossesion(forDevice: ESPDevice, completionHandler: @escaping (String) -> Void) {
////        let connectVC = self.storyboard?.instantiateViewController(withIdentifier: "connectVC") as! ConnectViewController
////        connectVC.espDevice = forDevice
////        connectVC.popHandler = completionHandler
////        self.navigationController?.pushViewController(connectVC, animated: true)
//
//
//        pop = "abcd1234"
//            Utility.showLoader(message: "Connecting to device", view: view)
//        forDevice.security = Utility.shared.espAppSettings.securityMode
//        forDevice.connect(delegate: self) { status in
//                DispatchQueue.main.async {
//                    Utility.hideLoader(view: self.view)
//                    switch status {
//                    case .connected:
//                        self.goToProvision()
//                    case let .failedToConnect(error):
//                        self.showStatusScreen(error: error)
//                    default:
//                        let action = UIAlertAction(title: "Retry", style: .default, handler: nil)
//                        self.showAlert(error: "Device disconnected", action: action)
//                    }
//            }
//        }
//    }
    
    func getProofOfPossesion(forDevice: ESPDevice, completionHandler: @escaping (String) -> Void) {
        //forDevice.security = Utility.shared.espAppSettings.securityMode
        self.espDevice = forDevice
        pop = "wiser123"
        
//            Utility.showLoader(message: "Connecting to device", view: view)
//               forDevice.security = Utility.shared.espAppSettings.securityMode
//               forDevice.connect(delegate: self) { status in
//                      DispatchQueue.main.async {
//                         Utility.hideLoader(view: self.view)
//                           switch status {
//                          case .connected:
//                              self.goToProvision()
//                         case let .failedToConnect(error):
//                           self.showStatusScreen(error: error)
//                          default:
//                         let action = UIAlertAction(title: "Retry", style: .default, handler: nil)
//                             self.showAlert(error: "Device disconnected", action: action)
//                         }
//              }
//           }
        completionHandler(pop)
    }
}



